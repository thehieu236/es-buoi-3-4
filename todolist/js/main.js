let form = document.querySelector('.card__content');
let input = document.getElementById('newTask');
let addItem = document.getElementById('addItem');

form.addEventListener('submit', (e) => {
	e.preventDefault();
	let val = input.value.trim();

	if (val) {
		addTodoElement({
			text: val,
		});

		saveTodoList();
	}

	input.value = '';
});

function addTodoElement(todo) {
	let li = document.createElement('li');

	li.innerHTML = `
        <span>${todo.text}</span>
        <div class="buttons">
            <button id="trash">
				<i class="remove far fa-trash-alt" aria-hidden="true"></i>
            </button>
            <button id="complete">
				<i class="complete fas fa-check-circle" aria-hidden="true"></i>
            </button>
        </div>
    `;

	// Remove todo element
	li.querySelector('#trash').addEventListener('click', (e) => {
		e.target.parentElement.parentElement.parentElement.remove();
		saveTodoList();
	});

	let idCompleted = document.getElementById('completed');
	// Add to completed
	li.querySelector('#complete').addEventListener('click', (e) => {
		let todoComplete = e.target.parentElement.parentElement.parentElement;
		todoComplete.classList.add('completed');

		idCompleted.appendChild(todoComplete);
		saveTodoList();
	});

	let todos = document.getElementById('todo');
	todos.appendChild(li);

	if (todo.status === 'completed') {
		li.classList.add('completed');

		idCompleted.appendChild(li);
	}
	saveTodoList();
}

function saveTodoList() {
	let todoList = document.querySelectorAll('li');
	let todoStorage = [];

	if (todoList.length == 0) {
		localStorage.setItem('todoList', JSON.stringify(todoStorage));
	} else {
		todoList.forEach((item) => {
			let text = item.querySelector('span').innerText;
			let status = item.getAttribute('class');
			todoStorage.push({
				text,
				status,
			});

			localStorage.setItem('todoList', JSON.stringify(todoStorage));
		});
	}
}

function init() {
	let data = JSON.parse(localStorage.getItem('todoList'));

	data.forEach(function (item) {
		addTodoElement(item);
	});
}

init();

function sortAZ() {
	let data = JSON.parse(localStorage.getItem('todoList'));
	data.sort((a, b) => {
		const nameA = a.text.toUpperCase(); // ignore upper and lowercase
		const nameB = b.text.toUpperCase(); // ignore upper and lowercase
		if (nameA < nameB) {
			return -1;
		}
		if (nameA > nameB) {
			return 1;
		}

		// names must be equal
		return 0;
	});

	clearTodoList();
	data.forEach(function (item) {
		addTodoElement(item);
	});
}

function sortZA() {
	let data = JSON.parse(localStorage.getItem('todoList'));
	data.sort((a, b) => {
		const nameA = a.text.toUpperCase(); // ignore upper and lowercase
		const nameB = b.text.toUpperCase(); // ignore upper and lowercase
		if (nameA > nameB) {
			return -1;
		}
		if (nameA < nameB) {
			return 1;
		}

		// names must be equal
		return 0;
	});

	clearTodoList();
	data.forEach(function (item) {
		addTodoElement(item);
	});
}

function clearTodoList() {
	let li = document.querySelectorAll('li');
	li.forEach((item) => item.remove());
}
